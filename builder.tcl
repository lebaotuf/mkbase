#!/usr/bin/env tclsh

set PS_ROOT "/root/pkgsrc"
set PATCH_ROOT "/root/patches"

proc apply_patches {cat name} {
    set cpath [pwd]
    cd $::PATCH_ROOT/${cat}/${name}
    exec -- >&@stdout ./apply $cpath
    cd $cpath
}

proc build_pkg {cat name} {
    set papply [file join ${::PATCH_ROOT} ${cat} ${name}]
    cd [file join ${::PS_ROOT} ${cat} ${name}]
    if {[file exists $papply]} {
	apply_patches $cat $name
    }
    exec -- >&@stdout bmake package package-install clean clean-depends
}

build_pkg devel git-base

