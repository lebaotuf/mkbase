#!/usr/bin/env tclsh

namespace eval bldr {
    proc info {out} {
	puts [string cat {[[BUILDER]]} " " $out]
    }

    proc make_mps {targetdir} {
	set cpath [pwd]
	cd $targetdir
	exec bmake mps
	cd $cpath
    }
}
