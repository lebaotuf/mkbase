** TEXTRELs and PIE
If your linker is complaining about creating DT_TEXTREL in a PIE, make
sure all your libraries (especially in the toolchain) are compiled
with PIC. Not having PIC libraries will get your hardened executables
(the ones linking against those libraries) compiled with -fpie
segfaulting on startup because musl cannot relocate them and the
loader will crash in do_relocs().

I hit this bug in 2022Q1, after upgrading zlib to 1.2.12, which broke
the configure script and did not build a shared zlib library, only a
static one without PIC. After building openssh and dropbear from
pkgsrc, both of them crashed with segfault on startup. These two
projects use hardened compiler options such as full RELRO and PIE
conflicting with the non-relocateable code in zlib and producing
broken binaries.
